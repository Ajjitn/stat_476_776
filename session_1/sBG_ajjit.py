import pandas as pd
import numpy as np
import scipy as sp
from scipy.optimize import minimize


###### Helper Functions ######
def calc_p_t(alpha, beta, t):
    # Function for calculating probability of churtning at time t,
    # given alpha and bta
    if t==0:
        return(np.nan)
    elif t == 1:
        return (alpha/(alpha+beta))
    else:
        return( (beta + t -2) / (alpha + beta + t - 1) * \
                     calc_p_t(alpha, beta, t-1))

def calc_ll(params, data):
    # Function for creating LL where
    # data = cleaned df with t and churned_x columns
    # params = [alpha, beta]

    alpha = params[0]
    beta = params[1]

    p_t = data['t'].apply(lambda x: calc_p_t(alpha, beta, x))

    num_survived = data['n_x'][0] - np.nansum(data['churned_x'])
    dead_ll = sum((np.log(p_t) * data['churned_x']).dropna())
    surv_ll = np.log(1 - np.nansum(p_t)) * num_survived

    return (dead_ll + surv_ll)

def clean_data(data, n_x_column_name, t_column_name):
    # Data cleaning utility function, requires:
    # n_x_column_name = name of column with number of customers
    # t = name of time period column

    data = data.rename(index=str, columns={n_x_column_name:"n_x",
                                     t_column_name:"t"})


    data['churned_x'] = data['n_x'].diff().abs()
    return(data[['t', 'n_x','churned_x']])

def fit_sbg_params(data):
    # Helper function to fit sBG params using optim

    optim = sp.optimize.minimize(
                            lambda x: -calc_ll(x, data),  # max LL = min (-LL)
                            x0=[1, 1],  # Starting values for optimization
                            method='nelder-mead',
                            tol=1e-6)

    alpha = optim['x'][0]
    beta = optim['x'][1]

    return ({"alpha":  optim['x'][0], "beta":optim['x'][1] })


### Data read in and parameter calculation ###
data_raw = pd.read_csv(
    'C:/Users/ajjit/Downloads/sBG_python_ryan_andrews/hw1_data.csv')
data = clean_data(data_raw, "Regular", "t")

params = fit_sbg_params(data)